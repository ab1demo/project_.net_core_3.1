﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using project_.net_core_3._1.BusinessLogic;

namespace test_.net_core_3._1.TestMethods
{
    class MathTestBL
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestSum()
        {
            var mathBL = new MathBL();
            int result = mathBL.Sum(2, 3);
            Assert.IsTrue(result == 5);
        }
    }
}
